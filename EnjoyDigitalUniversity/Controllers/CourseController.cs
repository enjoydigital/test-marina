﻿using System.Web.Mvc;
using EnjoyDigitalUniversity.Models;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using System.Web;
using EnjoyDigitalUniversity.ViewModels;

namespace EnjoyDigitalUniversity.Controllers
{
    public class CourseController : SurfaceController
    {
        public CourseController()
        {
        }

        [ChildActionOnly]
        //Because of what's written on the hint, I tried to get the children of the current page (according to the documentation this allows
        //to execute a portion of the rendered area of a view, which in this case I'm guessing it's the div in Listing.cshtml)
        //https://our.umbraco.com/documentation/reference/templating/mvc/child-actions
        //Also there seems to get a bug on this line, but it's taken from the documentation and I don't seem able to find the solution
        public PartialViewResult Listing(string department)
        {
            var model = new CoursesViewModel();
            //Here I tried to get the properties of the CourseViewModel class as they appear on Listing and in the query on line 24
            //however even with extensive research I'm not yet proficient enough in c# to get this to work.
            model.Department = department.Department;
            model.Url = department.Url;
         }  

            //Hint get current pages children, map to list of courses and populate CourseViewModel
            //var courses = CurrentPage.
            //I believe I should have included this last variable. Even though CurrentPage is quite self-explanatory, it's not clear to me how I would populate it.

            //My reasoning behind this is that in CourseListing.cshtml there is a JS button that when activated should make a list of courses appear,
            //like the one displayed in Listing. This should appear in the "form-output" div.
            //To make this happen we need to make a controller that takes the relevant information, based on the classes already set up,
            //like for example what's in CourseListingViewModel.cs and displays this on the front end (given that it's a surface controller).

        public PartialViewResult Detail()
        {
            var model = Map(CurrentPage);

            return PartialView(model);
        }

        public PartialViewResult ApplyForm()
        {
            var student = new Student
            {
                CourseId = CurrentPage.Id
            };

            return PartialView(student);
        }

        public ActionResult Apply(Student student)
        {            
            return CurrentUmbracoPage();
        }

        public CourseViewModel Map(IPublishedContent content)
        {
            var course = new CourseViewModel();

            course.Id = content.Id;
            course.BodyText = content.GetPropertyValue<IHtmlString>("bodyText");
            return course;
        }
    }
}