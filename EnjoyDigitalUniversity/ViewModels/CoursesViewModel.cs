﻿using System.Collections.Generic;
using EnjoyDigitalUniversity.Models;

namespace EnjoyDigitalUniversity.ViewModels
{
    public class CoursesViewModel
    {
        public IEnumerable<CourseViewModel> Courses { get; set; }

        public string Department { get; set; }
    }    
}